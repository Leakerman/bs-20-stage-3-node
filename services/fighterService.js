const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  search(search) {
    const item = FighterRepository.getOne(search);
    console.log('fdsgfd',item);

    if(!item) {
      throw Error("Fighter not found")
    }
    return item 
  }

  getAll() {
    const fighters = FighterRepository.getAll();

    if(!fighters.length) {
      throw Error("Fighters not found")
    }
    return fighters;
  }

  create(fighter) {
    const isCreated = FighterRepository.create(fighter);
    if (!isCreated) {
      throw Error("Fighter was not created");
    }
    return isCreated;
  }

  update(id, newFighter) {
    if(this.search({id})) {
      return FighterRepository.update(id, newFighter)
    } else {
      throw Error("Wrong id");
    }
  }

  delete(id) {
    const deletedItems = FighterRepository.delete(id);
    if (!deletedItems.length) {
      throw Error("Wrong id");
    }
    return deletedItems;
  }
}

module.exports = new FighterService();
