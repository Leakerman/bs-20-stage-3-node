const { UserRepository } = require("../repositories/userRepository");

class UserService {
  search(search) {
    const item = UserRepository.getOne(search);
    console.log('fdsgfd',item);
    
    if(!item) {
      throw Error("User not found")
    }
    return item 
  }

  getAll() {
    const users = UserRepository.getAll();

    if(!users.length) {
      throw Error("Users not found")
    }
    return users;
  }

  create(user) {
    const isCreated = UserRepository.create(user);
    if (!isCreated) {
      throw Error("User was not created");
    }
    return isCreated
  }

  update(id, newUser) {
    if(this.search({id})) {
      return UserRepository.update(id, newUser)
    } else {
      throw Error("Wrong id");
    }
  }

  delete(id) {
    const deletedItems = UserRepository.delete(id);
    if (!deletedItems.length) {
      throw Error("Wrong id");
    }
    return deletedItems;
  }
}

module.exports = new UserService();
