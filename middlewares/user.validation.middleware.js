const { user } = require("../models/user");
const createUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    const { email, phoneNumber, password } = userCandidate;

    hasExtraProperties(userCandidate, user);
    hasAllProperties(userCandidate, user);
    phoneNumberCheck(phoneNumber);
    passwordCheck(password);
    gmailCheck(email);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  try {
    const userCandidate = req.body;
    const { email, phoneNumber, password } = userCandidate;

    hasExtraProperties(userCandidate, user);
    if (phoneNumber) phoneNumberCheck(phoneNumber);
    if (password) passwordCheck(password);
    if (email) gmailCheck(email);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

function hasExtraProperties(user, model) {
  for (let key in user) {
    if (!model.hasOwnProperty(key) || key === "id") {
      throw Error("User body has an extra fields");
    }
  }
}

function hasAllProperties(user, model) {
  const isValid = Object.keys(model).reduce((result, key) => {
    return key === "id" ? result : result && user.hasOwnProperty(key);
  }, true);

  if (!isValid) throw Error("User body is missing fields");
}

function passwordCheck(password) {
  if (password.length <= 3)
    throw Error("Password must have mor than 3 symbols");
}

function gmailCheck(gmail) {
  const regex = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;
  if (!regex.test(gmail)) throw Error("Invalid gmail address");
}

function phoneNumberCheck(number) {
  const regex = /^\+38(0\d{9})$/;
  if (!regex.test(number)) throw Error("Invalid phone number");
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
