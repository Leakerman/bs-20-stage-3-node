const responseMiddleware = (req, res, next) => {
  if (res.data) {
    res.status(200).json(res.data);
  } else {
    const {err} = res

    res.status(res.code || 400).json({
      error: true,
      message: err.message || err
    });
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
