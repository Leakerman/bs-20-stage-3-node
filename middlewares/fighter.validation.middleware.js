const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  try {
    const fighterCandidate = req.body;
    const { defense, power } = fighterCandidate;

    hasExtraProperties(fighterCandidate, fighter);
    hasAllProperties(fighterCandidate, fighter);
    defenseCheck(defense);
    powerCheck(power);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  try {
    const fighterCandidate = req.body;
    const { defense, power } = fighterCandidate;

    hasExtraProperties(fighterCandidate, fighter);
    if (defense) defenseCheck(defense);
    if (power) powerCheck(power);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

function powerCheck(power) {
  if (+power > 100) throw Error("Power level must be less than 100");
}

function defenseCheck(defense) {
  if (+defense > 10 || +defense < 1)
    throw Error("Defense level must be from 1 to 10");
}

function hasExtraProperties(fighter, model) {
  for (let key in fighter) {
    if (!model.hasOwnProperty(key) || key === "id") {
      throw Error("Fighter body has an extra fields");
    }
  }
}

function hasAllProperties(fighter, model) {
  const isValid = Object.keys(model).reduce((result, key) => {
    return key === "id" ? result : result && fighter.hasOwnProperty(key);
  }, true);
  console.log(isValid);

  if (!isValid) throw Error("Fighter body is missing fields");
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
